import { hm_db, work_db } from '../dbUtils/createConnections';
import { generateDropDatesConditions } from '../dbUtils/queries';

async function start() {
    console.log('Start testing....');
    const errors = [];
    await hm_db.connect();
    await work_db.connect();

    const dropDatesCondition = generateDropDatesConditions();

    const playersFromHmDb = await hm_db.many(`select playername from compiledplayerresults
        join players on players.player_id = compiledplayerresults.player_id
        join gametypes on gametypes.gametype_id = compiledplayerresults.gametype_id
        where istourney = false ${dropDatesCondition}
        group by playername`);
    const playersFromWorkDb = await work_db.many('select playername from mycompiledplayerresults group by playername');

    if (playersFromWorkDb.length !== playersFromHmDb.length) {
        errors.push(`count players error - playersFromHmDb: ${playersFromHmDb.length}, playerFromWorkDb: ${playersFromWorkDb.length}`);
    }

    for (let i = 0; i < playersFromHmDb.length; i += 1) {
        const player = playersFromHmDb[i];
        const playername = player.playername;
        console.log(`${i} / ${playersFromHmDb.length} - ${playername}`);

        const resultFromHmDb = await hm_db.one(`select sum(totalamountwonincents) as wins, sum(totalhands) as hands from compiledplayerresults 
        join players on players.player_id = compiledplayerresults.player_id 
        join gametypes on gametypes.gametype_id = compiledplayerresults.gametype_id
        where playername = $1 and istourney = false ${dropDatesCondition}
        `, [playername]);
        const resultFromWorkDb = await work_db.one('select sum(totalamountwonincents) as wins, sum(totalhands) as hands from mycompiledplayerresults where playername = $1', [playername]);

        if (+resultFromHmDb.wins !== +resultFromWorkDb.wins) {
            errors.push(`wins error - playername: ${playername}, winFromHmDb: ${resultFromHmDb.wins}, winFromWorkDb: ${resultFromWorkDb.wins} `);
        }

        if (+resultFromHmDb.hands !== +resultFromWorkDb.hands) {
            errors.push(`wins error - playername: ${playername}, handsFromHmDb: ${resultFromHmDb.hands}, hadnsFromWorkDb: ${resultFromWorkDb.hands} `);
        }
    }

    if (errors.length === 0) {
        console.log('All ok');
    } else {
        console.log('errors:', errors);
    }
    process.exit();
}

start();
