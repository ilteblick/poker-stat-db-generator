import { generateGetPlayersCountQuery, generateGetPlayersChunckQuery, generateGetOneCompiledResultQuery, generateCopyOneCompiledResultQuery } from './queries';
import { hm_db, work_db } from './createConnections';

const limit = 100;

export async function copyCompiledResults() {
    const getPlayersCountQuery = generateGetPlayersCountQuery();

    const getPlayerCountResult = await hm_db.one(getPlayersCountQuery.query)
        .catch(() => {
            console.log('Error: No players in HM compiledplayerresults');
        });

    if (getPlayerCountResult) {
        const copyRequestsCount = Math.floor(getPlayerCountResult.count / 100) + 1;


        for (let i = 0; i < copyRequestsCount; i += 1) {
            const offset = limit * i;
            const getPlayersChunkQuery = generateGetPlayersChunckQuery(limit, offset);

            const players = await hm_db.many(getPlayersChunkQuery.query, getPlayersChunkQuery.params)
                .catch(() => {
                    console.log('Error: No compiledresults in this chunck');
                });

            if (players) {
                for (const player of players) {
                    console.log(`${player.player_id} / ${getPlayerCountResult.count} - ${player.playername} `);
                    const getOneCompiledPlayerResultQuery = generateGetOneCompiledResultQuery(player);

                    const formatedCompiledPlayerResult = await hm_db.many(
                        getOneCompiledPlayerResultQuery.query,
                        getOneCompiledPlayerResultQuery.params,
                    ).catch(() => {
                        console.log(`Error: No compiledresults in HM compiledplayerresults (${player.player_id})`);
                    });

                    if (formatedCompiledPlayerResult) {
                        await work_db.tx(async (t) => {
                            const copyQueries = formatedCompiledPlayerResult.map((x) => {
                                const copyOneCompiledResultQuery = generateCopyOneCompiledResultQuery(x, player);
                                return t.none(copyOneCompiledResultQuery.query, copyOneCompiledResultQuery.params);
                            });

                            await t.batch(copyQueries)
                                .catch(() => { });
                        });
                    }
                }
            }
        }
    }
}
