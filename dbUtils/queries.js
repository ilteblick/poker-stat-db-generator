export function generateDropDatesConditions() {
    return process.env.DROP_DATES === 'true'
        ? `and compiledplayerresults.playedyearandmonth = any(ARRAY[${process.env.YEAR_AND_MONTH},${process.env.YEAR_AND_MONTH_2}])`
        : '';
}

export function generateGetPlayersCountQuery() {
    const dropDatesCondition = generateDropDatesConditions();

    const query = `
        select count(*) from (
        select player_id from compiledplayerresults 
            join gametypes on compiledplayerresults.gametype_id = gametypes.gametype_id
            where istourney = false ${dropDatesCondition}
            group by player_id) as a
    `;

    return {
        query,
        params: [],
    };
}

export function generateGetPlayersChunckQuery(limit, offset) {
    const dropDatesCondition = generateDropDatesConditions();

    const query = `
        select compiledplayerresults.player_id, playername, pokersite_id from compiledplayerresults 
        join players on players.player_id = compiledplayerresults.player_id
        join gametypes on compiledplayerresults.gametype_id = gametypes.gametype_id
        where istourney = false ${dropDatesCondition}
        group by compiledplayerresults.player_id, playername, pokersite_id order by player_id limit $1 offset $2
    `;

    return {
        query,
        params: [limit, offset],
    };
}

export function generateGetOneCompiledResultQuery(compiledResult) {
    const dropDatesCondition = generateDropDatesConditions();

    const query = `
        select playername, gametypes.tablesize, gametypes.bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id, pokersite_id,
            
        sum( totalhands ) as  totalhands,
        sum( totalamountwonincents ) as  totalamountwonincents,
        sum( totalrakeincents ) as  totalrakeincents,
        sum( totalbbswon ) as  totalbbswon,
        sum( vpiphands ) as  vpiphands,
        sum( pfrhands ) as  pfrhands,
        sum( couldcoldcall ) as  couldcoldcall,
        sum( didcoldcall ) as  didcoldcall,
        sum( couldthreebet ) as  couldthreebet,
        sum( didthreebet ) as  didthreebet,
        sum( couldsqueeze ) as  couldsqueeze,
        sum( didsqueeze ) as  didsqueeze,
        sum( facingtwopreflopraisers ) as  facingtwopreflopraisers,
        sum( calledtwopreflopraisers ) as  calledtwopreflopraisers,
        sum( raisedtwopreflopraisers ) as  raisedtwopreflopraisers,
        sum( smallblindstealattempted ) as  smallblindstealattempted,
        sum( smallblindstealdefended ) as  smallblindstealdefended,
        sum( smallblindstealreraised ) as  smallblindstealreraised,
        sum( bigblindstealattempted ) as  bigblindstealattempted,
        sum( bigblindstealdefended ) as  bigblindstealdefended,
        sum( bigblindstealreraised ) as  bigblindstealreraised,
        sum( sawnonsmallshowdown ) as  sawnonsmallshowdown,
        sum( wonnonsmallshowdown ) as  wonnonsmallshowdown,
        sum( sawlargeshowdown ) as  sawlargeshowdown,
        sum( wonlargeshowdown ) as  wonlargeshowdown,
        sum( sawnonsmallshowdownlimpedflop ) as  sawnonsmallshowdownlimpedflop,
        sum( wonnonsmallshowdownlimpedflop ) as  wonnonsmallshowdownlimpedflop,
        sum( sawlargeshowdownlimpedflop ) as  sawlargeshowdownlimpedflop,
        sum( wonlargeshowdownlimpedflop ) as  wonlargeshowdownlimpedflop,
        sum( wonhand ) as  wonhand,
        sum( wonhandwhensawflop ) as  wonhandwhensawflop,
        sum( wonhandwhensawturn ) as  wonhandwhensawturn,
        sum( wonhandwhensawriver ) as  wonhandwhensawriver,
        sum( facedthreebetpreflop ) as  facedthreebetpreflop,
        sum( foldedtothreebetpreflop ) as  foldedtothreebetpreflop,
        sum( calledthreebetpreflop ) as  calledthreebetpreflop,
        sum( raisedthreebetpreflop ) as  raisedthreebetpreflop,
        sum( facedfourbetpreflop ) as  facedfourbetpreflop,
        sum( foldedtofourbetpreflop ) as  foldedtofourbetpreflop,
        sum( calledfourbetpreflop ) as  calledfourbetpreflop,
        sum( raisedfourbetpreflop ) as  raisedfourbetpreflop,
        sum( turnfoldippassonflopcb ) as  turnfoldippassonflopcb,
        sum( turncallippassonflopcb ) as  turncallippassonflopcb,
        sum( turnraiseippassonflopcb ) as  turnraiseippassonflopcb,
        sum( riverfoldippassonturncb ) as  riverfoldippassonturncb,
        sum( rivercallippassonturncb ) as  rivercallippassonturncb,
        sum( riverraiseippassonturncb ) as  riverraiseippassonturncb,
        sum( sawflop ) as  sawflop,
        sum( sawshowdown ) as  sawshowdown,
        sum( wonshowdown ) as  wonshowdown,
        sum( totalbets ) as  totalbets,
        sum( totalcalls ) as  totalcalls,
        sum( flopcontinuationbetpossible ) as  flopcontinuationbetpossible,
        sum( flopcontinuationbetmade ) as  flopcontinuationbetmade,
        sum( turncontinuationbetpossible ) as  turncontinuationbetpossible,
        sum( turncontinuationbetmade ) as  turncontinuationbetmade,
        sum( rivercontinuationbetpossible ) as  rivercontinuationbetpossible,
        sum( rivercontinuationbetmade ) as  rivercontinuationbetmade,
        sum( facingflopcontinuationbet ) as  facingflopcontinuationbet,
        sum( foldedtoflopcontinuationbet ) as  foldedtoflopcontinuationbet,
        sum( calledflopcontinuationbet ) as  calledflopcontinuationbet,
        sum( raisedflopcontinuationbet ) as  raisedflopcontinuationbet,
        sum( facingturncontinuationbet ) as  facingturncontinuationbet,
        sum( foldedtoturncontinuationbet ) as  foldedtoturncontinuationbet,
        sum( calledturncontinuationbet ) as  calledturncontinuationbet,
        sum( raisedturncontinuationbet ) as  raisedturncontinuationbet,
        sum( facingrivercontinuationbet ) as  facingrivercontinuationbet,
        sum( foldedtorivercontinuationbet ) as  foldedtorivercontinuationbet,
        sum( calledrivercontinuationbet ) as  calledrivercontinuationbet,
        sum( raisedrivercontinuationbet ) as  raisedrivercontinuationbet,
        sum( totalpostflopstreetsseen ) as  totalpostflopstreetsseen,
        sum( totalaggressivepostflopstreetsseen ) as  totalaggressivepostflopstreetsseen 
        from compiledplayerresults 
            join gametypes on gametypes.gametype_id = compiledplayerresults.gametype_id
            join players on players.player_id = compiledplayerresults.player_id
            where players.player_id = $1 and istourney = false ${dropDatesCondition}
            group by playedyearandmonth, gametypes.tablesize, gametypes.bigblindincents, gametypes.currencytype_id, pokergametype_id, playername, pokersite_id
            order by playedyearandmonth
    `;

    return {
        query,
        params: [compiledResult.player_id],
    };
}

export function generateCopyOneCompiledResultQuery(result, player) {
    const query = `
        INSERT INTO mycompiledplayerresults
        (playername, pokersite_id, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id,
            totalhands,
            totalamountwonincents,
            totalrakeincents,
            totalbbswon,
            vpiphands,
            pfrhands,
            couldcoldcall,
            didcoldcall,
            couldthreebet,
            didthreebet,
            couldsqueeze,
            didsqueeze,
            facingtwopreflopraisers,
            calledtwopreflopraisers,
            raisedtwopreflopraisers,
            smallblindstealattempted,
            smallblindstealdefended,
            smallblindstealreraised,
            bigblindstealattempted,
            bigblindstealdefended,
            bigblindstealreraised,
            sawnonsmallshowdown,
            wonnonsmallshowdown,
            sawlargeshowdown,
            wonlargeshowdown,
            sawnonsmallshowdownlimpedflop,
            wonnonsmallshowdownlimpedflop,
            sawlargeshowdownlimpedflop,
            wonlargeshowdownlimpedflop,
            wonhand,
            wonhandwhensawflop,
            wonhandwhensawturn,
            wonhandwhensawriver,
            facedthreebetpreflop,
            foldedtothreebetpreflop,
            calledthreebetpreflop,
            raisedthreebetpreflop,
            facedfourbetpreflop,
            foldedtofourbetpreflop,
            calledfourbetpreflop,
            raisedfourbetpreflop,
            turnfoldippassonflopcb,
            turncallippassonflopcb,
            turnraiseippassonflopcb,
            riverfoldippassonturncb,
            rivercallippassonturncb,
            riverraiseippassonturncb,
            sawflop,
            sawshowdown,
            wonshowdown,
            totalbets,
            totalcalls,
            flopcontinuationbetpossible,
            flopcontinuationbetmade,
            turncontinuationbetpossible,
            turncontinuationbetmade,
            rivercontinuationbetpossible,
            rivercontinuationbetmade,
            facingflopcontinuationbet,
            foldedtoflopcontinuationbet,
            calledflopcontinuationbet,
            raisedflopcontinuationbet,
            facingturncontinuationbet,
            foldedtoturncontinuationbet,
            calledturncontinuationbet,
            raisedturncontinuationbet,
            facingrivercontinuationbet,
            foldedtorivercontinuationbet,
            calledrivercontinuationbet,
            raisedrivercontinuationbet,
            totalpostflopstreetsseen,
            totalaggressivepostflopstreetsseen)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
            $11, $12, $13, $14, $15, $16, $17, $18, $19, $20,
            $21, $22, $23, $24, $25, $26, $27, $28, $29, $30,
            $31, $32, $33, $34, $35, $36, $37, $38, $39, $40,
            $41, $42, $43, $44, $45, $46, $47, $48, $49, $50,
            $51, $52, $53, $54, $55, $56, $57, $58, $59, $60,
            $61, $62, $63, $64, $65, $66, $67, $68, $69, $70,
            $71, $72, $73, $74, $75, $76, $77, $78, $79)
    `;

    const params = [player.playername, player.pokersite_id, result.tablesize,
    result.bigblindincents, result.playedyearandmonth,
    result.currencytype_id, result.pokergametype_id,
    result.totalhands,
    result.totalamountwonincents,
    result.totalrakeincents,
    result.totalbbswon,
    result.vpiphands,
    result.pfrhands,
    result.couldcoldcall,
    result.didcoldcall,
    result.couldthreebet,
    result.didthreebet,
    result.couldsqueeze,
    result.didsqueeze,
    result.facingtwopreflopraisers,
    result.calledtwopreflopraisers,
    result.raisedtwopreflopraisers,
    result.smallblindstealattempted,
    result.smallblindstealdefended,
    result.smallblindstealreraised,
    result.bigblindstealattempted,
    result.bigblindstealdefended,
    result.bigblindstealreraised,
    result.sawnonsmallshowdown,
    result.wonnonsmallshowdown,
    result.sawlargeshowdown,
    result.wonlargeshowdown,
    result.sawnonsmallshowdownlimpedflop,
    result.wonnonsmallshowdownlimpedflop,
    result.sawlargeshowdownlimpedflop,
    result.wonlargeshowdownlimpedflop,
    result.wonhand,
    result.wonhandwhensawflop,
    result.wonhandwhensawturn,
    result.wonhandwhensawriver,
    result.facedthreebetpreflop,
    result.foldedtothreebetpreflop,
    result.calledthreebetpreflop,
    result.raisedthreebetpreflop,
    result.facedfourbetpreflop,
    result.foldedtofourbetpreflop,
    result.calledfourbetpreflop,
    result.raisedfourbetpreflop,
    result.turnfoldippassonflopcb,
    result.turncallippassonflopcb,
    result.turnraiseippassonflopcb,
    result.riverfoldippassonturncb,
    result.rivercallippassonturncb,
    result.riverraiseippassonturncb,
    result.sawflop,
    result.sawshowdown,
    result.wonshowdown,
    result.totalbets,
    result.totalcalls,
    result.flopcontinuationbetpossible,
    result.flopcontinuationbetmade,
    result.turncontinuationbetpossible,
    result.turncontinuationbetmade,
    result.rivercontinuationbetpossible,
    result.rivercontinuationbetmade,
    result.facingflopcontinuationbet,
    result.foldedtoflopcontinuationbet,
    result.calledflopcontinuationbet,
    result.raisedflopcontinuationbet,
    result.facingturncontinuationbet,
    result.foldedtoturncontinuationbet,
    result.calledturncontinuationbet,
    result.raisedturncontinuationbet,
    result.facingrivercontinuationbet,
    result.foldedtorivercontinuationbet,
    result.calledrivercontinuationbet,
    result.raisedrivercontinuationbet,
    result.totalpostflopstreetsseen,
    result.totalaggressivepostflopstreetsseen,
    ];

    return {
        query,
        params,
    };
}
