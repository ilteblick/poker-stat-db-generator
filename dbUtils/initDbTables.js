import { hm_db, work_db } from './createConnections';

export async function initDbTables() {
    await hm_db.connect();
    await work_db.connect();

    if (process.env.DROP_MINING_STATS === 'true') {
        await work_db.any('DROP table IF EXISTS playerresults cascade');
    }

    await work_db.any(`create table playerresults(
        playername text,
        pokersite_id smallint,
        hand_id bigint,
        result double precision,
        PRIMARY KEY (playername,pokersite_id,hand_id)
    )`).catch(() => { });

    if (process.env.DROP_MINING_STATS === 'true') {
        await work_db.any('DROP table IF EXISTS hands cascade');
    }

    await work_db.any(`create table hands(
        hand_id bigint,
        date bigint,
        hm_format_date bigint,
        
        currencytype_id smallint,
        pokergametype_id smallint,
        pokersite_id smallint,
        bigblindincents integer,
        tablesize smallint,
        PRIMARY KEY (pokersite_id,hand_id)
    )`).catch(() => { });

    await work_db.any('DROP table IF EXISTS mycompiledplayerresults cascade');

    await work_db.any(`create table mycompiledplayerresults(
        id serial,
        playername text,
        pokersite_id smallint,

        tablesize smallint,
        bigblindincents integer,
        playedyearandmonth integer,
        currencytype_id smallint,
        pokergametype_id smallint,
        
        totalhands integer NOT NULL,
        totalamountwonincents integer NOT NULL,
        totalrakeincents integer NOT NULL,
        totalbbswon integer NOT NULL,
        vpiphands integer NOT NULL,
        pfrhands integer NOT NULL,
        couldcoldcall integer NOT NULL,
        didcoldcall integer NOT NULL,
        couldthreebet integer NOT NULL,
        didthreebet integer NOT NULL,
        couldsqueeze integer NOT NULL,
        didsqueeze integer NOT NULL,
        facingtwopreflopraisers integer NOT NULL,
        calledtwopreflopraisers integer NOT NULL,
        raisedtwopreflopraisers integer NOT NULL,
        smallblindstealattempted integer NOT NULL,
        smallblindstealdefended integer NOT NULL,
        smallblindstealreraised integer NOT NULL,
        bigblindstealattempted integer NOT NULL,
        bigblindstealdefended integer NOT NULL,
        bigblindstealreraised integer NOT NULL,
        sawnonsmallshowdown integer NOT NULL,
        wonnonsmallshowdown integer NOT NULL,
        sawlargeshowdown integer NOT NULL,
        wonlargeshowdown integer NOT NULL,
        sawnonsmallshowdownlimpedflop integer NOT NULL,
        wonnonsmallshowdownlimpedflop integer NOT NULL,
        sawlargeshowdownlimpedflop integer NOT NULL,
        wonlargeshowdownlimpedflop integer NOT NULL,
        wonhand integer NOT NULL,
        wonhandwhensawflop integer NOT NULL,
        wonhandwhensawturn integer NOT NULL,
        wonhandwhensawriver integer NOT NULL,
        facedthreebetpreflop integer NOT NULL,
        foldedtothreebetpreflop integer NOT NULL,
        calledthreebetpreflop integer NOT NULL,
        raisedthreebetpreflop integer NOT NULL,
        facedfourbetpreflop integer NOT NULL,
        foldedtofourbetpreflop integer NOT NULL,
        calledfourbetpreflop integer NOT NULL,
        raisedfourbetpreflop integer NOT NULL,
        turnfoldippassonflopcb integer NOT NULL,
        turncallippassonflopcb integer NOT NULL,
        turnraiseippassonflopcb integer NOT NULL,
        riverfoldippassonturncb integer NOT NULL,
        rivercallippassonturncb integer NOT NULL,
        riverraiseippassonturncb integer NOT NULL,
        sawflop integer NOT NULL,
        sawshowdown integer NOT NULL,
        wonshowdown integer NOT NULL,
        totalbets integer NOT NULL,
        totalcalls integer NOT NULL,
        flopcontinuationbetpossible integer NOT NULL,
        flopcontinuationbetmade integer NOT NULL,
        turncontinuationbetpossible integer NOT NULL,
        turncontinuationbetmade integer NOT NULL,
        rivercontinuationbetpossible integer NOT NULL,
        rivercontinuationbetmade integer NOT NULL,
        facingflopcontinuationbet integer NOT NULL,
        foldedtoflopcontinuationbet integer NOT NULL,
        calledflopcontinuationbet integer NOT NULL,
        raisedflopcontinuationbet integer NOT NULL,
        facingturncontinuationbet integer NOT NULL,
        foldedtoturncontinuationbet integer NOT NULL,
        calledturncontinuationbet integer NOT NULL,
        raisedturncontinuationbet integer NOT NULL,
        facingrivercontinuationbet integer NOT NULL,
        foldedtorivercontinuationbet integer NOT NULL,
        calledrivercontinuationbet integer NOT NULL,
        raisedrivercontinuationbet integer NOT NULL,
        totalpostflopstreetsseen integer NOT NULL,
        totalaggressivepostflopstreetsseen integer NOT NULL
    )`);

    // if (process.env.DROP_MINING_STATS === 'true') {
    //     await work_db.any('DROP table IF EXISTS additionalplayerresults cascade');
    // }

    // await work_db.any(`create table additionalplayerresults (
    //     --id serial,
    //     playername text,
    //     pokersite_id smallint,

    //     tablesize smallint,
    //     bigblindincents integer,
    //     playedyearandmonth integer,
    //     currencytype_id smallint,
    //     pokergametype_id smallint,

    //     flopbetmade integer default 0,
    //     flopbetpossible integer default 0,

    //     flopraisemade integer default 0,
    //     flopraisepossible integer default 0,

    //     flopcallmade integer default 0,
    //     flopcallpossible integer default 0,

    //     flopcallvsbetmade integer default 0,
    //     flopraisevsbetmade integer default 0,
    //     flopfoldvsbetmade integer default 0,

    //     turnbetmade integer default 0,
    //     turnbetpossible integer default 0,

    //     turnraisemade integer default 0,
    //     turnraisepossible integer default 0,

    //     turncallmade integer default 0,
    //     turncallpossible integer default 0,

    //     turncallvsbetmade integer default 0,
    //     turnraisevsbetmade integer default 0,
    //     turnfoldvsbetmade integer default 0,

    //     riverbetmade integer default 0,
    //     riverbetpossible integer default 0,

    //     riverraisemade integer default 0,
    //     riverraisepossible integer default 0,

    //     rivercallmade integer default 0,
    //     rivercallpossible integer default 0,

    //     rivercallvsbetmade integer default 0,
    //     riverraisevsbetmade integer default 0,
    //     riverfoldvsbetmade integer default 0,
    //     PRIMARY KEY (playername,pokersite_id)
    // )`).catch(() => { });

    if (process.env.DROP_MINING_STATS === 'true') {
        await work_db.any('DROP table IF EXISTS preadditionalplayerresults cascade');
    }

    await work_db.any(`create table preadditionalplayerresults (
        id serial,
        playername text,
        pokersite_id smallint,

        tablesize smallint,
        bigblindincents integer,
        playedyearandmonth integer,
        currencytype_id smallint,
        pokergametype_id smallint,



        flopbetmade integer default 0,
        flopbetpossible integer default 0,

        flopraisemade integer default 0,
        flopraisepossible integer default 0,

        flopcallmade integer default 0,
        flopcallpossible integer default 0,

        flopcallvsbetmade integer default 0,
        flopraisevsbetmade integer default 0,
        flopfoldvsbetmade integer default 0,

        turnbetmade integer default 0,
        turnbetpossible integer default 0,

        turnraisemade integer default 0,
        turnraisepossible integer default 0,

        turncallmade integer default 0,
        turncallpossible integer default 0,

        turncallvsbetmade integer default 0,
        turnraisevsbetmade integer default 0,
        turnfoldvsbetmade integer default 0,

        riverbetmade integer default 0,
        riverbetpossible integer default 0,

        riverraisemade integer default 0,
        riverraisepossible integer default 0,

        rivercallmade integer default 0,
        rivercallpossible integer default 0,

        rivercallvsbetmade integer default 0,
        riverraisevsbetmade integer default 0,
        riverfoldvsbetmade integer default 0
    )`).catch(() => { });
}
