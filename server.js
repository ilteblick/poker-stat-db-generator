import { initDbTables } from './dbUtils/initDbTables';
import { copyCompiledResults } from './dbUtils/copyCompiledResults';

async function start() {
    await initDbTables();
    await copyCompiledResults();

    console.log('Successful generate database');
    process.exit();
}

start();

